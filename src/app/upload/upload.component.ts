import { Component, OnInit, ViewChild } from '@angular/core';
import { UploadService } from '../service/upload.service';
import { Job } from '../domain/job';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  @ViewChild('file', {static: false }) file;
  jobs: Job[];
  uploadFile: File;
  filesPending = false;
  fileStatus = 'Add';

  constructor(public uploadService: UploadService) { }

  ngOnInit() {
    this.getFiles();
  }

  getFiles() {
    this.uploadService.getFiles().subscribe((value) => {
      this.jobs = value;
      this.jobs.forEach((job) => {
        if (job.status !== 'transcribed'){
          this.filesPending = true;
        }
      });
    });
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  onFileAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files ) {
      if (!isNaN(parseInt(key))) {
        this.uploadFile = files[key];
        this.fileStatus = 'Change';
      }
    }
  }

  uploadingFile() {
  }

}
