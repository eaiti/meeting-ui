import { Component, OnInit, ViewChild } from '@angular/core';
import { EmotionService } from '../service/emotion.service';
import { Emotion } from '../domain/emotion';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';

import { ActivatedRoute } from '@angular/router';
import emotionMapEngage from './engage-handoff.json';
import emotionMapPlg from './plg-demo.json';
import emotionMapTote from './tote-meeting.json';
import emotionMapYusen from './yusen-discovery.json';

@Component({
  selector: 'app-emotions',
  templateUrl: './emotions.component.html',
  styleUrls: ['./emotions.component.scss']
})
export class EmotionsComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  public emotionMap = {};
  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(255,255,0,0.2)',
      borderColor: 'rgba(255,255,0,1)',
      pointBackgroundColor: 'rgba(255,255,0,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: 'rgba(255,255,0,1)',
      pointHoverBorderColor: 'rgba(255,255,0,0.8)'
    },
    {
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: 'rgba(77,83,96,1)',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    {
      backgroundColor: 'rgba(255,159,177,0.2)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(255,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: 'rgba(255,159,177,1)',
      pointHoverBorderColor: 'rgba(255,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(0,255,0,0.2)',
      borderColor: 'green',
      pointBackgroundColor: 'rgba(0,255,0,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: 'rgba(0,255,0,1)',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(255,0,255,0.2)',
      borderColor: 'rgba(255,0,255,0.8)',
      pointBackgroundColor: 'rgba(255,0,255,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: 'rgba(255,0,255,1)',
      pointHoverBorderColor: 'rgba(255,0,255,0.8)'
    },
    {
      backgroundColor: 'rgba(0,0,255,0.4)',
      borderColor: 'blue',
      pointBackgroundColor: 'rgba(0,0,255,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: 'rgba(0,0,255,1)',
      pointHoverBorderColor: 'rgba(0,0,255,0.8)',
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public fileId: string;

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(
    public emotionService: EmotionService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.fileId = this.route.snapshot.paramMap.get('id');

    switch (this.fileId) {
      case '0UKUbphl0pxZ':
        this.emotionMap = emotionMapEngage;
        break;
      case 'f8kZHVzOpRBA':
        this.emotionMap = emotionMapYusen;
        break;
      case 'aphtVsFeW0ei':
        this.emotionMap = emotionMapTote;
        break;
      case 'a6ka9XoVwR5t':
        this.emotionMap = emotionMapPlg;
        break;
    }

    const emotionCount = this.initEmotionCount();
    for (const time in this.emotionMap) {
      if (time) {
        const detectedEmotions = this.emotionMap[time].emotions.detected;
        ['joy', 'surprise', 'disgust', 'sadness', 'anger', 'fear'].forEach(emotion => {
          const prev = emotionCount[emotion][emotionCount[emotion].length - 1];
          if (detectedEmotions.indexOf(emotion) !== -1) {
            emotionCount[emotion].push(prev + 1);
          } else {
            emotionCount[emotion].push(prev);
          }
        });
      }
    }

    this.lineChartData = [
      // tslint:disable:no-string-literal
      { data: emotionCount['joy'], label: 'joy', lineTension: 0 },
      { data: emotionCount['surprise'], label: 'surprise', lineTension: 0 },
      { data: emotionCount['disgust'], label: 'disgust', lineTension: 0 },
      { data: emotionCount['sadness'], label: 'sadness', lineTension: 0 },
      { data: emotionCount['anger'], label: 'anger', lineTension: 0 },
      { data: emotionCount['fear'], label: 'fear', lineTension: 0 }
    ];
    this.lineChartLabels = Array(Object.keys(this.emotionMap).length).fill(1).map((e, i) => `Minute ${i}`);
  }

  getEmotions() {
    this.emotionService.getEmotions().subscribe((value) => {

    });
  }

  public initEmotionCount() {
    const emotionCount = {};

    emotionCount['joy'] = [0];
    emotionCount['surprise'] = [0];
    emotionCount['disgust'] = [0];
    emotionCount['sadness'] = [0];
    emotionCount['anger'] = [0];
    emotionCount['fear'] = [0];

    return emotionCount;
  }
}
