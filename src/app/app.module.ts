import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule, MatDialogModule, MatListModule, MatProgressBarModule,
  MatFormFieldModule, MatGridListModule, MatTabsModule, MatToolbarModule} from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UploadComponent } from './upload/upload.component';
import { UploadService } from './service/upload.service';
import { SummaryComponent } from './summary/summary.component';
import { SummaryService } from './service/summary.service';
import { ResultsComponent } from './results/results.component';
import { EmotionsComponent } from './emotions/emotions.component';
import { TranscriptComponent } from './transcript/transcript.component';

const appRoutes: Routes = [
  { path: 'upload', component: UploadComponent },
  { path: 'results/:id', component: ResultsComponent },
  { path: 'emotions', component: EmotionsComponent },
  { path: '**', redirectTo: '/upload'}
];

@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    SummaryComponent,
    ResultsComponent,
    EmotionsComponent,
    TranscriptComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    ChartsModule,
    MatButtonModule,
    MatDialogModule,
    MatListModule,
    FlexLayoutModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatTabsModule,
    MatToolbarModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  entryComponents: [],
  providers: [UploadService, SummaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
