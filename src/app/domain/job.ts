export interface Job {
    completed_on: Date;
    created_on: Date;
    duration_seconds: number;
    id: string;
    metadata: string;
    name: string;
    status: string;
    type: string;
}
