export interface Snippet {
    speaker: string;
    startTime: string;
    endTime: string;
    text: string;
}
