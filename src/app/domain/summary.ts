import { Snippet } from './snippet';
import { QuestAns } from './quest-ans';

export interface Summary {
    highlights: string[];
    questions: string[];
    actions: string[];
}
