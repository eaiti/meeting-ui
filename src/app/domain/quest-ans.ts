import { Snippet } from './snippet';

export interface QuestAns {
    question: Snippet;
    answer?: Snippet;
}
