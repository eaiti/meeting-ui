import { Component, OnInit } from '@angular/core';
import { SummaryService } from '../service/summary.service';
import { Summary } from '../domain/summary';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import sample1 from '../0UKUbphl0pxZ.json';
import sample2 from '../f8kZHVzOpRBA.json';
import sample3 from '../aphtVsFeW0ei.json';
import sample4 from '../a6ka9XoVwR5t.json';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  summaryReport: Summary;
  fileId: string;

  constructor(
    public summaryService: SummaryService,
    private route: ActivatedRoute,
    private router: Router) { }


  ngOnInit() {
    this.fileId = this.route.snapshot.paramMap.get('id');
    switch(this.fileId){
      case '0UKUbphl0pxZ':
        this.summaryReport = sample1;
        break;
      case 'f8kZHVzOpRBA':
        this.summaryReport = sample2;
        break;
      case 'aphtVsFeW0ei':
        this.summaryReport = sample3;
        break;
      case 'a6ka9XoVwR5t':
        this.summaryReport = sample4;
        break;
    }
  }
}
