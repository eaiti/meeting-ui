import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Snippet } from '../domain/snippet';

@Injectable({
  providedIn: 'root'
})
export class TranscriptService {
  constructor(private http: HttpClient) { }

  public getTranscript(fileId: string) {
    return this.http.get<{transcript: Snippet[]}>('api/transcript/' + fileId).pipe(
      map((res) => res.transcript)
    );
  }
}
