import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Emotion } from '../domain/emotion';

@Injectable({
  providedIn: 'root'
})
export class EmotionService {

  constructor(private http: HttpClient) { }

  public getEmotions() {
    return this.http.get('api/emotions').pipe(

    );
  }
}
