import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Job } from '../domain/job';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }

  public getFiles() {
    return this.http.get<{jobs: Job[]}>('api/jobs').pipe(
      map((res) => res.jobs)
    );
  }
}
