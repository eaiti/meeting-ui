import { Component, OnInit } from '@angular/core';
import { TranscriptService } from '../service/transcript.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Snippet } from '../domain/snippet';

@Component({
  selector: 'app-transcript',
  templateUrl: './transcript.component.html',
  styleUrls: ['./transcript.component.scss']
})
export class TranscriptComponent implements OnInit {
  fileId: string;
  transcript: Snippet[];

  constructor(
    public transcriptService: TranscriptService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.fileId = this.route.snapshot.paramMap.get('id');
    this.getTranscript(this.fileId);
  }

  getTranscript(fileId: string) {
    this.transcriptService.getTranscript(fileId).subscribe((value) => {
      this.transcript = value;
    });
  }

  getMinute(time) {
    const seconds = time / 60;

    const minute = Math.floor(seconds);
    const second = Math.round(seconds / 60 % 1 * 60);

    return second < 10 ? `${minute}:0${second}` : `${minute}:${second}`;
}

}
